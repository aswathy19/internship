<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
		<h2 class="col-sm-offset-2">Login</h2>
		<h4><span id="status" class="label label-danger col-sm-offset-2" role="alert">${message?html}</span></h4>
		<div class="form-horizontal">
			<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
				<div class="col-sm-4">
					<input type="text" id="username" value="" class="form-control" id="inputEmail3" required placeholder="Username">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-4">
					<input type="password" id="password" class="form-control" id="inputPassword3" required placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button id="login" class="btn btn-success">Login</button>
				</div>
			</div>


	<script src="public/js/jquery-2.1.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="public/js/login.js"></script>

</body>
</html>
