<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
    		<h1 class="col-sm-offset-2">Something went wrong</h1>
    		<h4><span class="label label-danger col-sm-offset-2" role="alert">${msg?html}</span></h4>
    		<h2 class="col-sm-offset-2">Go to <a href="/">Home</a></h2>
    	</div>
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
