<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome {{person.username}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
    		<h2 class="col-sm-offset-2">Welcome ${person.username?html}!!</h2>
    		<button id="logout" class="col-sm-offset-2 btn btn-danger">Logout</button>
    	</div>

	<script src="public/js/jquery-2.1.3.min.js"></script>
            	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
            	<script src="public/js/logout.js"></script>
</body>
</html>