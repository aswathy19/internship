$(document).ready(function () {
	$('#join').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "" && $('#confirm').val().trim() !== "") {
			$.post( "/join", {
				username: $('#username').val(),
				password: $('#password').val(),
				confirm: $('#confirm').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/login';
				} else {
					$('#status').text(data);
				}
			});
		} else {
			$('#status').text("Please fill all the fields.");
		}
	});
});