$(document).ready(function () {
	$('#login').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "") {
			$.post( "/login", {
				username: $('#username').val(),
				password: $('#password').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/';
				} else {
					$('#status').text(data);
				}
			});
		} else {
			$('#status').text("Please fill all the fields.");
		}
	});
});
