package com.qburst.dropwizard.db;

import com.datastax.driver.core.*;
import com.qburst.dropwizard.configuration.MainConfiguration;
import com.qburst.dropwizard.core.Person;
import com.qburst.dropwizard.core.mappers.PersonMapper;

import java.util.ArrayList;

/**
 * Created by aswathy on 5/5/15.
 */
public class MyDao {
    Cluster cluster;
    Session session;

    public MyDao(MainConfiguration configuration) {
        this.cluster = Cluster.builder().addContactPoint(configuration.getCassandra().getHost()).build();
        this.session = this.cluster.connect(configuration.getCassandra().getKeyspace());
    }

    protected void finalize() {
        cluster.close();
    }

    public ArrayList<Person> login(String username, String password) {
        PreparedStatement statement = session.prepare("SELECT id from person where username = :username AND password = :password ALLOW FILTERING;");
        BoundStatement boundStatement = new BoundStatement(statement);
        return PersonMapper.map(session.execute(boundStatement.bind(username, password)));
    }

    public ArrayList<Person> duplicate(String username) {
            PreparedStatement statement = session.prepare("SELECT id FROM person WHERE username = ?;");
        BoundStatement boundStatement = new BoundStatement(statement);
        return PersonMapper.map(session.execute(boundStatement.bind(username)));
    }

    public Boolean insert(Person person) {
        PreparedStatement statement = session.prepare("INSERT INTO person(id, username, password) VALUES (?, ?, ?);");
        BoundStatement boundStatement = new BoundStatement(statement);
        return session.execute(boundStatement.bind(person.getId(), person.getUsername(), person.getPassword())).wasApplied();
    }
}
