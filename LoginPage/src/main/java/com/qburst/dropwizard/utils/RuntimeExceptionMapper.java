package com.qburst.dropwizard.utils;

import com.qburst.dropwizard.view.ErrorView;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by aswathy on 27/4/15.
 */
@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {
    public Response toResponse(RuntimeException exception) {
        Response response = Response
                .serverError()
                .entity(new ErrorView(exception))
                .build();
        return response;
    }
}
