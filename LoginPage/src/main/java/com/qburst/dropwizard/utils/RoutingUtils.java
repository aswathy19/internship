package com.qburst.dropwizard.utils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.net.URI;
/**
 * Created by aswathy on 27/4/15.
 */
public class RoutingUtils {
    public static Response redirectToURI(String newURI) {
        URI uri = null;
        try {
            uri = new URI(newURI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
         return Response.seeOther(uri).build();

    }
}
