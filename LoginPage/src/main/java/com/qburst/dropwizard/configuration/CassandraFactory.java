package com.qburst.dropwizard.configuration;

/**
 * Created by aswathy on 5/5/15.
 */
public class CassandraFactory {
    private String host;
    private String keyspace;

    public void setHost(String host) {
        this.host = host;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getHost() {
        return host;
    }

    public String getKeyspace() {
        return keyspace;
    }
}
