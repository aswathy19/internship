package com.qburst.dropwizard.application;

;

import com.qburst.dropwizard.db.MyDao;
import com.qburst.dropwizard.configuration.MainConfiguration;
import com.qburst.dropwizard.resource.PersonResource;
import com.qburst.dropwizard.utils.RuntimeExceptionMapper;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;
import org.skife.jdbi.v2.DBI;

import java.util.Map;

/**
 * Created by aswathy on 23/4/15.
 */

public class MainApplication extends Application<MainConfiguration> {
   public static void main(String[] args) throws Exception {
        new MainApplication().run(args);
    }
    public void initialize(Bootstrap<MainConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<MainConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(MainConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", null));
        bootstrap.addBundle(new AssetsBundle("/assets/favicon.ico", "/assets/favicon.ico", null, "favicon"));
    }

    @Override
    public void run(MainConfiguration configuration, Environment environment)  {


        final MyDao dao = new MyDao(configuration);

        final PersonResource resource = new PersonResource(dao);
        environment.jersey().register( resource);
        environment.servlets().setSessionHandler(new SessionHandler());

    }
}
