package com.qburst.dropwizard.view;

import io.dropwizard.views.View;

/**
 * Created by aswathy on 27/4/15.
 */
public class LoginView extends View {
    private final String username;
   private final String message;
    public LoginView(String username) {
        super("login.ftl");
        this.username = username;
        this.message="";
    }
    public LoginView(String username,String message) {
        super("login.ftl");
        this.username = username;
        this.message=message;
    }
    public String getUsername() {
        return username;
    }
    public String getMessage() {
        return message;
    }
}
