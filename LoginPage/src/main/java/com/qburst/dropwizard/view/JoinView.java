package com.qburst.dropwizard.view;

import io.dropwizard.views.View;

/**
 * Created by aswathy on 27/4/15.
 */
public class JoinView extends View {
    private final String username;
    private final String message;


    public JoinView(String username) {
        super("join.ftl");
        this.username = username;
        this.message="";

    }
    public JoinView(String username,String message) {
        super("join.ftl");
        this.username = username;
        this.message=message;
    }
    public String getUsername() {
        return username;
    }
    public String getMessage(){return message;}

}
