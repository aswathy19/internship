package com.qburst.dropwizard.view;

import com.qburst.dropwizard.core.Person;
import io.dropwizard.views.View;

/**
 * Created by aswathy on 27/4/15.
 */
public class PersonView extends View {
    private Person person;

    public PersonView(Person person) {
        super("person.ftl");
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
