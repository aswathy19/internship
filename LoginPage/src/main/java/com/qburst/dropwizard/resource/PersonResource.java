package com.qburst.dropwizard.resource;

import com.qburst.dropwizard.core.Person;
import com.qburst.dropwizard.db.MyDao;
import com.qburst.dropwizard.utils.HashingUtils;
import com.qburst.dropwizard.utils.RoutingUtils;
import com.qburst.dropwizard.view.HomeView;
import com.qburst.dropwizard.view.JoinView;
import com.qburst.dropwizard.view.LoginView;
import com.qburst.dropwizard.view.PersonView;
import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;

//import com.qburst.dropwizard.view.MyView;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by sreeraj on 22/4/15.
 */
@Path("/")
@Produces(MediaType.TEXT_HTML)
public class PersonResource {
    private final MyDao dao;

    public PersonResource(MyDao dao) {
        this.dao = dao;
    }
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Object getPerson(@Session HttpSession session) {
        String sessionAttribute = ((String) session.getAttribute("username"));
        if (sessionAttribute == null || sessionAttribute.isEmpty()) {
           return RoutingUtils.redirectToURI("/home");
        }

        return new PersonView(new Person((String) session.getAttribute("id"), (String) session.getAttribute("username"),""));
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)

    public boolean logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.setAttribute("id", "");
        session.invalidate();
        return true;

    }

    @Path("/login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView getLogin() {
        return new LoginView("");
    }

    @Path("/login")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String postLogin(@FormParam("username") String username, @FormParam("password") String password, @Session HttpSession session) {
        if (username.trim().isEmpty() || password.trim().isEmpty()) {

            return "please fill all the fields";
        }
        List<Person> listLogin = dao.login(username,HashingUtils.GenerateHash(password));
        System.out.println(listLogin.size());
        if (!listLogin.isEmpty()) {
            session.setAttribute("username", username);
            session.setAttribute("id", listLogin.get(0).getId());

            return "true";
        }
        System.out.println("empty");


        return "enter valid records";
    }



    @Path("/join")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinView getJoin() {
        return new JoinView("");
    }
    @Path("/home")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public HomeView gethome() {
        return new HomeView("");
    }

    @Path("/join")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String postJoin(@FormParam("username") String username, @FormParam("password") String password, @FormParam("confirm") String confirm) {
        if(username.trim().isEmpty()||username==null||password.trim().isEmpty()||confirm.trim().isEmpty())
        {

            return  "enter complete details!!";
        }
        if (username.length() > 30) {

            return "Username must NOT be longer that 30 characters.";
        }
        Pattern p = Pattern.compile("[^a-zA-Z]");
        if (username.contains(" ") || p.matcher(username).find()) {

            return "Username must NOT have space or special characters.";
        }

        if (password.length() < 6) {

            return "Password must have atleast 6 characters.";
        }
        List<Person> listJoin = dao.duplicate(username);
        System.out.println(listJoin.size());
        if (listJoin.isEmpty()) {

            if (password.equals(confirm)) {
                final Person newPerson = new Person(UUID.randomUUID().toString(),username, HashingUtils.GenerateHash(password));
                dao.insert(newPerson);

                return "true";
            }
            else
            {

                return "incorrect password";
            }
        }



        return "user name already exists";
    }




}