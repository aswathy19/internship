package com.qburst.dropwizard.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * Created by aswathy on 24/4/15.
 */
public class Person {
    @NotNull
    @JsonProperty("id")
    private String id;

    @NotNull
    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    public Person() {

    }

    public Person(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
