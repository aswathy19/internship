package com.qburst.dropwizard.core.mappers;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.Row;
import com.qburst.dropwizard.core.Person;
import org.apache.commons.beanutils.BeanUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import com.datastax.driver.core.ResultSet;
import java.lang.reflect.InvocationTargetException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aswathy on 27/4/15.
 */
public class PersonMapper  {
    public static ArrayList<Person> map(com.datastax.driver.core.ResultSet results) {
        ArrayList<Person> list = new ArrayList<Person>();
        List<ColumnDefinitions.Definition> columns = results.getColumnDefinitions().asList();

        for (Row row : results) {
            Person person = new Person();
            for (ColumnDefinitions.Definition col : columns) {
                try {
                    BeanUtils.setProperty(person, col.getName(), row.getString(col.getName()));
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            list.add(person);
        }

        return list;
    }
        }
